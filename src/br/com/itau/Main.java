package br.com.itau;

import java.util.Random;

public class Main {

    public static void main(String[] args) {


        Rodada rodada = new Rodada();
        RodadaHistorica rodadaHistorica = new RodadaHistorica();

        do {

            rodada.limparCartas();

            Captura.iniciaJogo(rodada);
            Sorteio.sorteioInicial(rodada);
            Captura.cartasNaMao(rodada);

            while (Captura.capturaDecisao(rodada)) {
                Sorteio.sortearUmaCarta(rodada);
                Captura.cartasNaMao(rodada);
            }

            Captura.mostraResultado(rodada);

            if(rodadaHistorica.validarSeHistorica(rodada.getSomaDasCartas())
                    && !rodada.jogadorGanhou()
                    && !rodada.jogadorPerdeu()){
                rodadaHistorica.setMelhorPontuacao(rodada.getSomaDasCartas());
                rodadaHistorica.setCartasSorteadas(rodada.getCartasSorteadas());
            }

        } while (!rodada.jogadorGanhou());

        System.out.println("Melhor rodada com " + rodadaHistorica.getMelhorPontuacao() + " pontos");
        rodadaHistorica.mostrarRodada();
    }
}
