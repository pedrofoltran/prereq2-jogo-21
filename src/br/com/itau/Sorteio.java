package br.com.itau;

import java.util.Random;

public class Sorteio {

    public static void sorteioInicial(Rodada rodada)
    {
        for(int i=0; i<2; i++){
            sortearUmaCarta(rodada);
        }

    }

    public static void sortearUmaCarta (Rodada rodada){
        Random aleatorio = new Random();
        Carta carta = new Carta(aleatorio.nextInt(53));

        while(rodada.cartaJaSorteada(carta)){
            carta = new Carta(aleatorio.nextInt(53));
        }

        rodada.adicionarCarta(carta);
    }
}
