package br.com.itau;

import java.util.List;

public class RodadaHistorica extends Rodada {
    private int melhorPontuacao;

    public boolean validarSeHistorica(int somaAtual){
        if (somaAtual > melhorPontuacao){
            return true;
        }
        return false;
    }

    public void setMelhorPontuacao(int melhorPontuacao) {
        this.melhorPontuacao = melhorPontuacao;
    }

    public int getMelhorPontuacao() {
        return melhorPontuacao;
    }
}
