package br.com.itau;


import java.sql.SQLOutput;
import java.util.Scanner;

public class Captura {
    public static boolean capturaDecisao(Rodada rodada) {
        Scanner captura = new Scanner(System.in);

        if (!rodada.jogadorGanhou() && !rodada.jogadorPerdeu()) {
            System.out.println(" ");
            System.out.println("Deseja pegar outra carta? S/N");
            String resposta = captura.next();
//            String resposta = "S";
            if (resposta.equals("S") || resposta.equals("s")) {
                return true;
            }
        }
        return false;
    }

    public static void iniciaJogo(Rodada cartasSorteadas){
        System.out.println("Olá, você irá jogar 21.");
        System.out.println("Sorteando suas duas primeiras cartas...");
        System.out.println(" ");

    }

    public static void cartasNaMao(Rodada rodada){
        System.out.println("Cartas em sua mão: ");
        rodada.mostrarRodada();
        System.out.println("Soma de pontos: " + rodada.getSomaDasCartas());
    }

    public static void mostraResultado(Rodada rodada) {
        if (rodada.jogadorPerdeu()){
            System.out.println("Você perdeu, seus pontos: " + rodada.getSomaDasCartas());
        } else if (rodada.jogadorGanhou()){
            System.out.println("Parabéns!!! VOCÊ GANHOU!");
        } else {
            System.out.println("Poxa, você não ganhou, nem perdeu. Sua pontuação: " + rodada.getSomaDasCartas());
        }
        System.out.println(" ");
        System.out.println(" ");
    }
}
