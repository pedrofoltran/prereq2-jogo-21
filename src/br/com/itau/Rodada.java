package br.com.itau;

import java.util.ArrayList;
import java.util.List;

public class Rodada {
    private List<Carta> cartasSorteadas = new ArrayList<Carta>();
    private int somaDasCartas;

    public Rodada(){}

    public void setCartasSorteadas(List<Carta> cartasSorteadas) {
        this.cartasSorteadas.addAll(cartasSorteadas);
    }

    public List<Carta> getCartasSorteadas() {
        return cartasSorteadas;
    }

    public boolean jogadorGanhou(){
        if(somaDasCartas == 21)
            return true;
        return false;
    }

    public boolean jogadorPerdeu(){
        if(somaDasCartas > 21){
            return true;
        }
        return false;
    }

    public void adicionarCarta(Carta carta){
        cartasSorteadas.add(carta);
        somaDasCartas += carta.getValor();
    }

    public boolean cartaJaSorteada(Carta carta){
        for(Carta cartaPesquisada: cartasSorteadas){
            if(cartaPesquisada.getNome().equals(carta.getNome()) &&
                    cartaPesquisada.getNaipe().equals(carta.getNaipe())){
                return true;
            }
        }
        return false;
    }

    public void limparCartas(){
        cartasSorteadas.clear();
        somaDasCartas = 0;
    }

    public int getSomaDasCartas() {
        return somaDasCartas;
    }

    public void mostrarRodada(){
        for(Carta cartaPesquisada: cartasSorteadas) {
            cartaPesquisada.mostrarCarta();
            System.out.println("");
        }
    }
}
