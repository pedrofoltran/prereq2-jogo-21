package br.com.itau;

public class Carta {
    private String nome;
    private String  naipe;
    private int valor;

    public Carta(int valorSorteio){

        if (valorSorteio < 13)
            this.naipe = "Paus";
        else if (valorSorteio >= 13 && valorSorteio < 26)
            this.naipe = "Espadas";
        else if (valorSorteio >= 26 && valorSorteio < 39)
            this.naipe = "Copas";
        else
            this.naipe = "Ouros";


        switch (valorSorteio % 13){
            case 0: this.nome = "A";
                    this.valor = 1;
                    break;
            case 10: this.nome = "Q";
                    this.valor = 10;
                    break;
            case 11: this.nome = "J";
                    this.valor = 10;
                    break;
            case 12: this.nome = "K";
                    this.valor = 10;
                    break;
            default: this.nome =  String.valueOf(valorSorteio%13+1);
                     this.valor = valorSorteio%13+1;

        }

    }

    public void mostrarCarta(){
        System.out.print("Carta: " + nome + " Naipe: " + naipe);
    }

    public String getNome() {
        return nome;
    }

    public String getNaipe() {
        return naipe;
    }

    public int getValor() {
        return valor;
    }
}
